import torch
import torch.optim as optim
import torchvision.transforms as transforms
from PIL import Image
import pickle
import os

from model.model import Prepared_Model


def load_images(image_name_list):
    size_list = []
    for i, name in enumerate(image_name_list):
        image = Image.open(name)
        size_list += list(image.size)
        image.close()
    loaders = []
    for i, name in enumerate(image_name_list):
        image = Image.open(name)
        if i == 1:
            loaders.append(transforms.Compose([transforms.ToTensor()])(image).unsqueeze(0).to(torch.float))
        else:
            size = (size_list[3], size_list[2])
            loaders.append(transforms.Compose([transforms.Resize(size),
                                               transforms.ToTensor()])(image).unsqueeze(0).to(torch.float))
        image.close()
    return loaders


def get_input_optimizer(input_img):
    # this line to show that input is a parameter that requires a gradient
    # добоваляет содержимое тензора катринки в список изменяемых оптимизатором параметров
    optimizer = optim.LBFGS([input_img.requires_grad_()])
    return optimizer


def MY_run_style_transfer(model, style_losses, content_losses, input_img, num_steps=500,
                          style_weight=100000, content_weight=1):
    """Run the style transfer."""

    optimizer = get_input_optimizer(input_img)
    run = [0]
    while run[0] <= num_steps:

        def closure():
            # correct the values
            # это для того, чтобы значения тензора картинки не выходили за пределы [0;1]
            input_img.data.clamp_(0, 1)

            optimizer.zero_grad()

            model(input_img)

            style_score = 0
            content_score = 0

            for sl in style_losses:
                style_score += sl.loss
            for cl in content_losses:
                content_score += cl.loss

            # взвешивание ощибки
            style_score *= style_weight
            content_score *= content_weight

            loss = style_score + content_score
            loss.backward()

            run[0] += 1
            return style_score + content_score

        optimizer.step(closure)

    # a last correction...
    input_img.data.clamp_(0, 1)

    return input_img


def create_image(path_to_images):
    """
    Функция создания картинки
    получает путь к папке с 2 картинками. 
    После работы модели сохраняет в эту папку полученную картинку.
    """
    img_path_list = [
        os.path.join(path_to_images, 'style_img.jpg'),
        os.path.join(path_to_images, 'content_img.jpg'),
    ]

    style_img, content_img = load_images(img_path_list)
    images = torch.cat((style_img, content_img))

    cnn_normalization_mean = torch.tensor([0.485, 0.456, 0.406])
    cnn_normalization_std = torch.tensor([0.229, 0.224, 0.225])

    model = Prepared_Model(cnn_normalization_mean, cnn_normalization_std)

    with open('model/model_params.pkl', 'rb') as dfile:
        params = pickle.load(dfile)
    model.load_state_dict(params)

    style_losses, content_losses = model.style_and_content_loss(images)

    input_img = content_img.clone()
    output = MY_run_style_transfer(model, style_losses, content_losses, input_img, num_steps=400)

    del model

    output = output.permute(0, 2, 3, 1)[0].detach() * 255
    output = output.byte().numpy()
    Image.fromarray(output).save(os.path.join(path_to_images, 'result.jpg'))
