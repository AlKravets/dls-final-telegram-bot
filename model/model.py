import torch
import torch.nn as nn
import torch.nn.functional as F


class ContentLoss(nn.Module):

    def __init__(self, target, ):
        super(ContentLoss, self).__init__()
        # we 'detach' the target content from the tree used
        # to dynamically compute the gradient: this is a stated value,
        # not a variable. Otherwise the forward method of the criterion
        # will throw an error.
        self.target = target.detach()  # это константа. Убираем ее из дерева вычеслений
        self.loss = F.mse_loss(self.target, self.target)  # to initialize with something

    def forward(self, input):
        self.loss = F.mse_loss(input, self.target)
        return input


def gram_matrix(input):
    batch_size, h, w, f_map_num = input.size()  # batch size(=1)
    # b=number of feature maps
    # (h,w)=dimensions of a feature map (N=h*w)

    features = input.view(batch_size * h, w * f_map_num)  # resise F_XL into \hat F_XL

    G = torch.mm(features, features.t())  # compute the gram product

    # we 'normalize' the values of the gram matrix
    # by dividing by the number of element in each feature maps.
    return G.div(batch_size * h * w * f_map_num)


class StyleLoss(nn.Module):
    def __init__(self, target_feature):
        super(StyleLoss, self).__init__()
        self.target = gram_matrix(target_feature).detach()
        self.loss = F.mse_loss(self.target, self.target)  # to initialize with something

    def forward(self, input):
        G = gram_matrix(input)
        self.loss = F.mse_loss(G, self.target)
        return input


class Normalization(nn.Module):
    def __init__(self, mean, std):
        super(Normalization, self).__init__()
        # .view the mean and std to make them [C x 1 x 1] so that they can
        # directly work with image Tensor of shape [B x C x H x W].
        # B is batch size. C is number of channels. H is height and W is width.
        self.mean = torch.tensor(mean).view(-1, 1, 1)
        self.std = torch.tensor(std).view(-1, 1, 1)

    def forward(self, img):
        # normalize img
        return (img - self.mean) / self.std


class Prepared_Model (nn.Module):
    def __init__(self, normalization_mean, normalization_std):
        super().__init__()
        self.n = Normalization(normalization_mean, normalization_std)
        self.conv_1 = nn.Conv2d(3, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.relu_1 = nn.ReLU()
        self.conv_2 = nn.Conv2d(64, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.relu_2 = nn.ReLU()
        self.pool_2 = nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
        self.conv_3 = nn.Conv2d(64, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.relu_3 = nn.ReLU()
        self.conv_4 = nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.relu_4 = nn.ReLU()
        self.pool_4 = nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
        self.conv_5 = nn.Conv2d(128, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))

    def style_and_content_loss(self,images):
        """
        images -- tensor with 2 images: styke and content
        images size = (2, 3, imsize, imsize)
        """
        style_losses = []
        content_losses = []

        images = self.n(images)
        images = self.conv_1(images)

        target_feature = images[0].clone().detach()
        self.style_loss_1 = StyleLoss(target_feature.unsqueeze(0))
        style_losses.append(self.style_loss_1)

        images = self.relu_1(images)
        images = self.conv_2(images)

        target_feature = images.clone().detach()
        self.style_loss_2 = StyleLoss(target_feature[0].unsqueeze(0))
        style_losses.append(self.style_loss_2)
        self.content_loss_2 = ContentLoss(target_feature[1].unsqueeze(0))
        content_losses.append(self.content_loss_2)

        images = self.relu_2(images)
        images = self.pool_2(images)
        images = self.conv_3(images)

        target_feature = images[0].clone().detach()
        self.style_loss_3 = StyleLoss(target_feature.unsqueeze(0))
        style_losses.append(self.style_loss_3)

        images = self.relu_3(images)
        images = self.conv_4(images)

        target_feature = images.clone().detach()
        self.style_loss_4 = StyleLoss(target_feature[0].unsqueeze(0))
        style_losses.append(self.style_loss_4)

        images = self.relu_4(images)
        images = self.pool_4(images)
        images = self.conv_5(images)

        target_feature = images[0].clone().detach()
        self.style_loss_5 = StyleLoss(target_feature.unsqueeze(0))
        style_losses.append(self.style_loss_5)

        return style_losses, content_losses

    def forward(self,x):
        x = self.n(x)
        x = self.conv_1(x)

        x = self.style_loss_1(x)

        x = self.relu_1(x)
        x = self.conv_2(x)

        x = self.content_loss_2(x)
        x = self.style_loss_2(x)

        x = self.relu_2(x)
        x = self.pool_2(x)
        x = self.conv_3(x)

        x = self.style_loss_3(x)

        x = self.relu_3(x)
        x = self.conv_4(x)

        x = self.style_loss_4(x)

        x = self.relu_4(x)
        x = self.pool_4(x)
        x = self.conv_5(x)

        x = self.style_loss_5(x)

        return x

