import logging
import os
import signal

from aiogram import Bot, Dispatcher, types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.utils.executor import start_webhook


import bot.messages
from bot.utils import make_user_dir, remove_user_data, execution, check_pid
from bot.settings import (BOT_TOKEN, WEBHOOK_URL, WEBHOOK_PATH,
                          WEBAPP_HOST, WEBAPP_PORT, DATA_DIR)

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
main_bot = Bot(token=BOT_TOKEN, parse_mode=types.ParseMode.HTML)
storage = MemoryStorage()
dp = Dispatcher(main_bot, storage=storage)

# Список состояний бота.
class OrderExec(StatesGroup):
    Before_Style_img_state = State()
    Before_Content_img_state = State()
    Before_exec_state = State()  # before execution
    Exec_state = State()  # run model and create res image


@dp.message_handler(commands=['help'], state='*')
async def help_message(message: types.Message, state: FSMContext):
    """
    Отправка help messages
    """
    current_state = await state.get_state() or 0
    await message.reply(bot.messages.help[current_state], parse_mode="HTML")


@dp.message_handler(commands=['start'])
async def start_message(message: types.Message, state: FSMContext):
    # Меняем состояние
    await OrderExec.Before_Style_img_state.set()
    await message.reply(bot.messages.start, parse_mode="HTML")

    # сохраняем user_id
    await state.update_data(user_id=message.from_user.id)
    # создаем папку в которую будут скачиваться картинки
    destination = os.path.join(DATA_DIR, str(message.from_user.id))
    make_user_dir(destination)


@dp.message_handler(commands=['cancel'], state='*')
async def cancel_message(message: types.Message, state: FSMContext):
    """
        Allow user to cancel any action
    """
    current_state = await state.get_state()
    if current_state is None:
        return
    data = await state.get_data()

    # Удаление директории пользователя с картинкими
    if 'user_id' in data.keys():
        remove_user_data(data['user_id'])

    # Завершение процесса вычислений
    if current_state == "OrderExec:Exec_state":
        if 'mod_pid' in data.keys() and check_pid(data['mod_pid']):
            os.kill(data['mod_pid'], signal.SIGKILL)

    # Cancel state and inform user about it
    logging.info('Cancelling state %r', current_state)
    await state.finish()
    await message.reply(bot.messages.cancel, reply_markup=types.ReplyKeyboardRemove(), parse_mode="HTML")



@dp.message_handler(content_types=['photo'], state=OrderExec.Before_Style_img_state)
async def save_style_img_message(message: types.Message, state: FSMContext):
    """
    Сохранение картинки стиля
    """
    await OrderExec.next()

    data = await state.get_data()
    user_id = data['user_id']
    destination = os.path.join(DATA_DIR, str(user_id))
    img_name = os.path.join(destination, 'style_img.jpg')
    await message.photo[-1].download(img_name, make_dirs=True)


@dp.message_handler(content_types=['photo'], state=OrderExec.Before_Content_img_state)
async def save_content_img_message(message: types.Message, state: FSMContext):
    """
    Сохранение картинки контента
    """
    data = await state.get_data()
    user_id = data['user_id']
    destination = os.path.join(DATA_DIR, str(user_id))
    img_name = os.path.join(destination, 'content_img.jpg')
    await message.photo[-1].download(img_name, make_dirs=True)

    await OrderExec.next()
    await message.answer(bot.messages.before_exec, parse_mode="HTML")


@dp.message_handler(commands=['exec'], state=OrderExec.Before_exec_state)
async def start_exec_message(message: types.Message, state: FSMContext):
    """
    Сообщение для старта вычислений
    """
    await OrderExec.next()
    await message.answer(bot.messages.exec, parse_mode="HTML")
    await execution(state)



@dp.message_handler(state='*', content_types=[types.ContentType.ANY])
async def for_all_message(message: types.Message, state: FSMContext):
    """
    Ответ на неожиданные сообщения
    """
    await message.reply(bot.messages.any)


async def on_startup(dp):
    await main_bot.set_webhook(WEBHOOK_URL)


def main():
    # executor.start_polling(dp, skip_updates=True)
    start_webhook(
        dispatcher=dp,
        webhook_path=WEBHOOK_PATH,
        on_startup=on_startup,
        skip_updates=True,
        host=WEBAPP_HOST,
        port=WEBAPP_PORT,
    )


if __name__ == '__main__':
    main()