import threading
import multiprocessing as mp
import asyncio
import os

from aiogram import Bot, types
from aiogram.dispatcher import FSMContext

from model.utils import create_image
from bot.settings import DATA_DIR, BOT_TOKEN


def make_user_dir(path):
    """
    Создание папки для хранения картинок
    """
    if not os.path.isdir(path):
        os.makedirs(path, exist_ok=True)


async def execution(state: FSMContext):
    """
    Функция для вычислений
    """
    Model_proc = threading.Thread(target=lambda state: asyncio.run(exec_decorator(state)), args=(state,))

    Model_proc.start()


async def exec_decorator(state: FSMContext):
    """
        Функция для избежания ошибки в heroku
        Она создает картинку (в отдельном процессе),
         отправляет ее клиенту и завершает состояния.
    """

    data = await state.get_data()
    user_id = data['user_id']
    destination = os.path.join(DATA_DIR, str(user_id))

    Model_proc = mp.Process(target=create_image, args=(destination,))

    Model_proc.start()
    mpid = Model_proc.pid
    await state.update_data(mod_pid=mpid)

    Model_proc.join()

    current_state = await state.get_state()
    if current_state != 'OrderExec:Exec_state':
        return
    # отправка картинки
    bot1 = Bot(token=BOT_TOKEN)
    img_path = os.path.join(DATA_DIR, str(user_id), 'result.jpg')

    await bot1.send_photo(user_id, types.InputFile(img_path))
    await bot1.close()

    # завершение работы
    remove_user_data(user_id)
    await state.finish()


def remove_user_data(user_id: int):
    """
    удаление папки пользователя с картинками
    """
    dir = os.path.join(DATA_DIR, str(user_id))
    if os.path.isdir(dir):
        for root, dirs, files in os.walk(dir, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))
        os.rmdir(dir)


def check_pid(pid):
    """ Check For the existence of a unix pid. """
    try:
        os.kill(pid, 0)
    except OSError:
        return False
    else:
        return True
